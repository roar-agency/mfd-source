import {NgModule} from "@angular/core";
import {MaterialModule} from "./material.module";
import {LayoutModule} from "@angular/cdk/layout";
import { CommonModule} from "@angular/common";
import {AppRoutesModule} from "./app-route.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
// import {AppRoutesModule} from "./app-route.module";

@NgModule({
  declarations: [

  ],
  imports: [
    CommonModule,
    MaterialModule,
    LayoutModule,
    AppRoutesModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  exports: [
    MaterialModule,
    LayoutModule,
    AppRoutesModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})

export class SharedModule {}
