import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {AdminPanelComponent} from "../components/admin-panel/admin-panel.component";
import {LoginComponent} from "../components/login/login.component";
import {OverviewComponent} from "../components/admin-panel/overview/overview.component";
import {StatusComponent} from "../components/admin-panel/status/status.component";
import {DealsComponent} from "../components/admin-panel/deals/deals.component";
import {NewOrderComponent} from "../components/admin-panel/overview/new-order/new-order.component";
import {ViewOrderComponent} from "../components/admin-panel/overview/view-order/view-order.component";



const appRoutes: Routes = [

  { path: '', redirectTo: '/login', pathMatch: 'full'},

  { path: 'admin', component: AdminPanelComponent, children: [

      {path: '', redirectTo: 'overview', pathMatch: 'full'},

      {path: 'overview', component: OverviewComponent,
        data: {
          animation: 'HomePage'
        }
      },

      {path: 'status', component: StatusComponent,
        data: {
          animation: 'StatusPage'
        }
      },

      {path: 'deals', component: DealsComponent,
        data: {
          animation: 'DealsPage'
        }
      },

      {path: 'new-order', component: NewOrderComponent,
        data: {
          animation: 'NewOrderPage'
        }
      },

      {path: 'view-order/:id', component: ViewOrderComponent,
        data: {
          animation: 'ViewOrderPage'
        }
      }
  ]},
  {path: 'login', component: LoginComponent}


];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {useHash: true})
  ],
  exports: [RouterModule],
  providers: []
})

export class AppRoutesModule { }
