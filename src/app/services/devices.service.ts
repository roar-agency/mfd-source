import {Injectable} from "@angular/core";
import {MainService} from "./main.service";
import {HttpClient} from "@angular/common/http";
import {Device} from "../models/device.type";

@Injectable()

export class DevicesService{

  apiUrl = this._main.getApiUrl();

  constructor(private _main: MainService,
              private _http: HttpClient) {}


  /** Returns all devices */

  get() {
    return this._http.get(this.apiUrl + '/devices')
      .toPromise().then((response: any) => {
        console.log(response);
        return JSON.parse(response._body);
      })
      .catch((reason => {
        console.log(reason);
      }));
  }

  /** Find one device by id */
  find(id) {
    console.log(id);

    let device: Device | any = {
      serial: 'Nimbus 2000',
      quantity: '200',
      warehouse: 'Wendimor',
      receivedLoc: 'Some Street',
      deliveryLoc: 'Some City',
      deliveryStatus: 'Status',
      poNumber: '01102',
      expectedDate: 'Jan 10, 2019'
    };

    return device;
  }

  /** Add device request */
  addDevice(device: Device, id: number) {

    console.log("Device");
    console.log(device, id);

    /** Device's payload in JSON format */
    const payload = {
      "accessories": device.accessories,
      "deviceModel": device.deviceModel,
      "quantity": device.quantity,
      "serial": device.serial
    };

    /** Request itself */
    return this._http.post(this.apiUrl + '/order/' + id + /devices/, payload)
      .toPromise().then((response: any) => {
        console.log(response);
        return JSON.parse(response._body);
      })
      .catch(reason => {
        console.log(reason);
      })
  }
  /** */

  /** Edit device request. Payload is different than one from create request
   * because of difference in layouts. Need to specify with Michael.
   * */
  editDevice(device, id) {
    const payload = {
      "serial": device.serial,
      "quantity": device.quantity,
      "warehouse": device.warehouse,
      "receivedLoc": device.receivedLoc,
      "deliveryLoc": device.deliveryLoc,
      "deliveryStatus": device.deliveryStatus,
      "poNumber": device.poNumber,
      "expectedDate": device.expectedDate
    };

    return this._http.put(this.apiUrl + '/devices/' + id, payload)
      .toPromise().then((response: any) => {
        console.log(response);
        return JSON.parse(response._body);
      })
      .catch((reason) => {
        console.log(reason);
      });

  }
  /** */

}
