import {Injectable} from "@angular/core";
import {MainService} from "./main.service";
import {HttpClient} from "@angular/common/http";
import {Order} from "../models/order.model";

@Injectable()

export class OrdersService{

  apiUrl = this._main.getApiUrl();


  constructor(private _main: MainService,
              private _http: HttpClient) {}


  /** Returns all orders */
  get() {
    return this._http.get(this.apiUrl + '/orders')
      .toPromise().then((response: any) => {
        console.log(response);
        return JSON.parse(response._body);
      })
      .catch((reason => {
        console.log(reason);
      }));
  }
  /** */

  /** Get order by id */
  getOrder(id: number): Order {

    const order = new Order(
      id,
      'Kivshovata',
      'Platon',
      1.5,
      'Gringots',
      'Goblin',
      'steak-0',
      'steak-0',
      'We have some',
      'Nice',
      'Jan 10, 2019',
      [],
      []);

    return order;
  }

 /** Create New Order */
 create(data) {

   console.log(data);

   /** Payload for order request */
   const payload = {
     "city" : data.city,
     "customer": data.customer,
     "funds": data.funds,
     "leadSource": data.leadSource,
     "paysTo": data.paysTo,
     "purchaseLease": data.purchaseLease,
     "rep": data.rep,
     "servicePlan": data.servicePlan,
     "type": data.type,
     "salesDate": data.salesDate,
     "devices": [],
     "trades": []
   };

   /** Request to create new order */
   return this._http.post(this.apiUrl + '/order/', payload)
     .toPromise().then((response: Order) => {
       console.log(response);
       return response;
     })
     .catch(reason => {
       console.log(reason);
     });
 }
 /** */

/** Edit order request */
  editOrder(order: Order, id: number) {

    console.log(order);

    /** Payload for order request */
    const payload = {
      "city" : order.city,
      "funds": order.funds,
      "leadSource": order.leadSource,
      "paysTo": order.paysTo,
      "rep": order.rep,
      "purchaseType": order.purchaseLease,
      "servicePlan": order.servicePlan,
      "type": order.type,
      "salesDate": order.salesDate
    };

    /** Check url */
    return this._http.put(this.apiUrl + '/order/' + id, payload)
      .toPromise().then((response: any) => {
        console.log(response);
      })
      .catch((reason: any) => {
        console.log(reason);
      });
  }
/** */

}
