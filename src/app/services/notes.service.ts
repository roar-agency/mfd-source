import {Injectable} from "@angular/core";
import {MainService} from "./main.service";
import {HttpClient} from "@angular/common/http";
import {Note} from "../models/note.model";

@Injectable()

export class NotesService{

  apiUrl = this._main.getApiUrl();

  constructor(private _main: MainService,
              private _http: HttpClient) {}


  /** Create note for order with id === note.order_id */
  create(note: Note ) {

    const payload = {
      "content": note.content,
      "createdBy": note.user_id
    };

    return this._http.post(this.apiUrl + '/orders/' + note.order_id + '/notes/', payload)
      .toPromise().then((response) => {
        console.log(response);
        return response;
      })
      .catch(reason => {
        console.log(reason);
      });
  }
  /** */

  
  /** Delete note with id */
  delete(id: number) {

    return this._http.delete(this.apiUrl + '/notes/' + id)
      .toPromise().then((response) => {
        console.log(response);
        return response;
      })
      .catch(reason => {
        console.log(reason);
      })
  }
  /** */



}
