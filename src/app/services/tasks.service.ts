import {Injectable} from "@angular/core";
import {MainService} from "./main.service";
import {HttpClient} from "@angular/common/http";
import {Task} from "../models/task.model";

@Injectable()

export class TasksService{

  apiUrl = this._main.getApiUrl();

  constructor(private _main: MainService,
              private _http: HttpClient) {}

  /** get all tasks */
  get() {

    return this._http.get(this.apiUrl + '/tasks')
      .toPromise().then((response: Task[]) => {
        console.log(response);
        return response;
      })
      .catch(reason => {
        console.log(reason);
      });
  }
  /** */

  /** Create task for order with id === task.order_id */
  create(task: Task) {

    const payload = {
      "device": task.device,
      "leaseTerm": task.leaseTerm,
      "dueDate": task.dueDate,
      "content": task.content,
      "assignBy": task.user_id,
      "status": task.status
    };

    return this._http.post(this.apiUrl + '/orders/' + task.order_id + '/tasks/', payload)
      .toPromise().then((response) => {
        console.log(response);
        return response;
      })
      .catch(reason => {
        console.log(reason);
      });
  }
  /** */

  /** Update task's status */
  update(id: number, status: boolean){

    const payload = {
      "status": status
    };

    return this._http.put(this.apiUrl + '/tasks/' + id, payload)
      .toPromise().then((response) => {
        console.log(response);
        return response;
      })
      .catch(reason => {
        console.log(reason);
      })
  }
  /** */

  /** Delete task with id */
  delete(id: number) {

    return this._http.delete(this.apiUrl + '/tasks/' + id)
      .toPromise().then((response) => {
        console.log(response);
        return response;
      })
      .catch(reason => {
        console.log(reason);
      })
  }
  /** */

}
