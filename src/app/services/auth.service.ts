import {Injectable} from "@angular/core";
import {MainService} from "./main.service";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Injectable()

export class AuthService{

  apiUrl: string = this._main.getApiUrl();

  constructor(private _main: MainService,
              private http: HttpClient,
              private _router: Router) {}

  login(email, pass) {
    console.log('Login Request: ' + email + ' ' + pass);

    const payload = {
      "email": email,
      "pass": pass
    };

    this._router.navigate(['/admin']);
    
    // return this.http.post(this.apiUrl + '/login', payload)
    //   .toPromise().then((response: any) => {
    //     console.log(response);
    //     return JSON.parse(response._body);
    //   })
    //   .catch(reason => {
    //     console.log(reason);
        // this._router.navigate(['/admin']);
    //   })
  }


  logOut() {
    console.log('Log out');
    this._router.navigate(['/login']);
  }


}
