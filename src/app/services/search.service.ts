import {Injectable} from "@angular/core";

@Injectable()

export class SearchService{

  constructor() {}

  // function that returns a filter for tables
  createFilter() {

    const filterFunction = (data, filter: string): boolean => {

      filter = filter.toLowerCase();

      const matchFilter = [];
      const filterArray = filter.split(',');
      // const columns = [data.id, data.company, data.advertiser, data.product];
      // Or if you don't want to specify specifics columns =>
      const columns = (<any>Object).values(data);

      // Main loop
      filterArray.forEach((fil) => {
        const customFilter = [];
        columns.forEach((column) => {
          customFilter.push(column.toLowerCase().includes(fil));
        });
        matchFilter.push(customFilter.some(Boolean)); // OR
      });
      return matchFilter.every(Boolean); // AND
    };
    return filterFunction;
  }
  // end of filter function
}
