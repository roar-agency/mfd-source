import {Injectable} from "@angular/core";
import {MainService} from "./main.service";
import {HttpClient} from "@angular/common/http";
import {Trade} from "../models/trade.type";

@Injectable()

export class TradesService{

  apiUrl = this._main.getApiUrl();

  constructor(private _main: MainService,
              private _http: HttpClient) {}


  /** Returns all trades */
  get() {
    return this._http.get(this.apiUrl + '/trades')
      .toPromise().then((response: any) => {
        console.log(response);
        return JSON.parse(response._body);
      })
      .catch((reason => {
        console.log(reason);
      }));
  }
  /** */

  /** Find one trade by id */
  find(id) {
    console.log(id);

    let trade: Trade | any = {
      model: 'Sharp MXC4002WC',
      serial: 'Some Serial',
      returnDisposition: 'steak-0',
      returnDate: 'Jan 19, 2019',
      funds: 'None',
      removedDate: 'May 19, 2019',
      blackMeter: '-----',
      colorMeter: '-----'
    };

    return trade;
  }

  /** Add trade request */
  addTrade(trade: Trade, id: number) {

    console.log("Trade");
    console.log(trade, id);

    /** Trade's payload in JSON */
    const payload = {
      "accessories": trade.accessories,
      "model": trade.model,
      "returnByDate": trade.returnByDate,
      "returnDisposition": trade.returnDisposition,
      "serial": trade.serial
    };

    return this._http.post(this.apiUrl + '/order/' + id + /trades/, payload)
      .toPromise().then((response: any) => {
        console.log(response);
        return JSON.parse(response._body);
      })
      .catch(reason => {
        console.log(reason);
      })
  }
  /** */


}
