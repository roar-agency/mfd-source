import {Injectable} from "@angular/core";
import {MainService} from "./main.service";
import {HttpClient} from "@angular/common/http";
import {Reminder} from "../models/reminder.model";

@Injectable()

export class RemindersService{

  apiUrl = this._main.getApiUrl();

  constructor(private _main: MainService,
              private _http: HttpClient) {}


  /** get all reminders */
  get() {

    return this._http.get(this.apiUrl + '/reminders')
      .toPromise().then((response: Reminder[]) => {
        console.log(response);
        return response;
      })
      .catch(reason => {
        console.log(reason);
      });
  }
  /** */

  /** Create reminder for order with id === reminder.order_id */
  create(reminder: Reminder ) {

    const payload = {
      "dueDate": reminder.date,
      "content": reminder.content,
      "createdBy": reminder.user_id,
    };

    return this._http.post(this.apiUrl + '/orders/' + reminder.order_id + '/reminders/', payload)
      .toPromise().then((response) => {
        console.log(response);
        return response;
      })
      .catch(reason => {
        console.log(reason);
      });
  }
  /** */


  /** Delete reminder with id */
  delete(id: number) {

    return this._http.delete(this.apiUrl + '/reminders/' + id)
      .toPromise().then((response) => {
        console.log(response);
        return response;
      })
      .catch(reason => {
        console.log(reason);
      })
  }
  /** */

}
