import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { LoginComponent } from './components/login/login.component';
import {SharedModule} from "./modules/shared.module";
import {OverviewComponent} from "./components/admin-panel/overview/overview.component";
import {StatusComponent} from "./components/admin-panel/status/status.component";
import {DealsComponent} from "./components/admin-panel/deals/deals.component";
import { OrdersComponent } from './components/admin-panel/overview/orders/orders.component';
import { TradesComponent } from './components/admin-panel/overview/trades/trades.component';
import { LeaseReportComponent } from './components/admin-panel/status/lease-report/lease-report.component';
import { PurchasedReportComponent } from './components/admin-panel/status/purchased-report/purchased-report.component';
import { DealsTableComponent } from './components/admin-panel/deals/deals-table/deals-table.component';
import { NewOrderComponent } from './components/admin-panel/overview/new-order/new-order.component';
import { FabButtonComponent } from './components/helpers/fab-button/fab-button.component';
import { WarningComponent } from './components/helpers/warning/warning.component';
import { ViewOrderComponent} from "./components/admin-panel/overview/view-order/view-order.component";
import { PurchasedOrdersComponent } from './components/admin-panel/overview/view-order/purchased-orders/purchased-orders.component';
import { LeasesComponent } from './components/admin-panel/overview/view-order/leases/leases.component';
import { EditModalComponent } from './components/admin-panel/overview/view-order/edit-modal/edit-modal.component';
import { EditDeviceModalComponent } from './components/admin-panel/overview/view-order/edit-device-modal/edit-device-modal.component';
import { EditTradeModalComponent } from './components/admin-panel/overview/view-order/edit-trade-modal/edit-trade-modal.component';
import { AddNoteModalComponent } from './components/admin-panel/overview/view-order/add-note-modal/add-note-modal.component';
import { AddReminderModalComponent } from './components/admin-panel/overview/view-order/add-reminder-modal/add-reminder-modal.component';
import { AddTaskModalComponent } from './components/admin-panel/overview/view-order/add-task-modal/add-task-modal.component';
import { AddPurchaseModalComponent } from './components/admin-panel/overview/view-order/add-purchase-modal/add-purchase-modal.component';
import { AddLeaseModalComponent } from './components/admin-panel/overview/view-order/add-lease-modal/add-lease-modal.component';

/** Services */
import {AuthService} from "./services/auth.service";
import {DevicesService} from "./services/devices.service";
import {NotesService} from "./services/notes.service";
import {OrdersService} from "./services/orders.service";
import {RemindersService} from "./services/reminders.service";
import {TasksService} from "./services/tasks.service";
import {TradesService} from "./services/trades.service";
import {MainService} from "./services/main.service";
import {SearchService} from "./services/search.service";

@NgModule({
  declarations: [
    AppComponent,
    AdminPanelComponent,
    LoginComponent,
    OverviewComponent,
    StatusComponent,
    DealsComponent,
    OrdersComponent,
    TradesComponent,
    LeaseReportComponent,
    PurchasedReportComponent,
    DealsTableComponent,
    NewOrderComponent,
    FabButtonComponent,
    WarningComponent,
    ViewOrderComponent,
    PurchasedOrdersComponent,
    LeasesComponent,
    EditModalComponent,
    EditDeviceModalComponent,
    EditTradeModalComponent,
    AddNoteModalComponent,
    AddReminderModalComponent,
    AddTaskModalComponent,
    AddPurchaseModalComponent,
    AddLeaseModalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule
  ],
  entryComponents: [
    EditModalComponent,
    EditTradeModalComponent,
    EditDeviceModalComponent,
    AddLeaseModalComponent,
    AddNoteModalComponent,
    AddPurchaseModalComponent,
    AddReminderModalComponent,
    AddTaskModalComponent
  ],
  providers: [
    AuthService,
    DevicesService,
    NotesService,
    OrdersService,
    RemindersService,
    TasksService,
    TradesService,
    MainService,
    SearchService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
