import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(
    private _router: Router,
  ) {}

  // decide if user in authenticated depends on token
  canActivate(): boolean {
    return true;
  }
}
