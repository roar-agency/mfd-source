import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { FormControl} from "@angular/forms";
import {SearchService} from "../../../../services/search.service";
import {OrdersService} from "../../../../services/orders.service";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})

export class OrdersComponent implements OnInit {

  displayedColumns: string[] = [
    'id',
    'name',
    'soDate',
    'customer',
    'rep',
    'status'
  ];
  // columns that we will display on front-end. we should have each of this ids in html template

  dataSource = new MatTableDataSource(ELEMENT_DATA);
  // data that will be displayed
  data: any[] = ELEMENT_DATA;

  positionFilter = new FormControl();

  constructor(private _search: SearchService,
              private _orders: OrdersService) {}

  ngOnInit() {

    this.getData();

    // subscribe to listen filter value
    this.positionFilter.valueChanges
      .subscribe(value => {
        this.dataSource.filter = value;
      });

    // custom filter function for table data:
    this.dataSource.filterPredicate = this._search.createFilter();
  }

  /** Get data to use it in table. Should get model for order later */
  getData() {

    this._orders.get()
      .then((response) => {
        console.log(response);
        // this.dataSource = response;
      });
  }


}

// declaration of data interface and data itself
// need to be changed when actual data will be available

export interface DataRow {
  id: string,
  name: string,
  soDate: string,
  customer: string,
  rep: string,
  status: string,
}

const ELEMENT_DATA: DataRow[] = [
  {
    id: '1',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  },
  {
    id: '2',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  },
  {
    id: '3',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  },  {
    id: '4',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  },  {
    id: '5',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  },  {
    id: '6',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  },  {
    id: '7',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  },  {
    id: '8',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  },
  {
    id: '9',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  },
  {
    id: '10',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  },  {
    id: '11',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  },  {
    id: '12',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  },  {
    id: '13',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  },  {
    id: '14',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  },  {
    id: '15',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  },
  {
    id: '16',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  },];

