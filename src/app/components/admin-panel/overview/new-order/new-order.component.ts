import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {OrdersService} from "../../../../services/orders.service";
import {Order} from "../../../../models/order.model";
import {Device} from "../../../../models/device.type";
import {Trade} from "../../../../models/trade.type";
import {DevicesService} from "../../../../services/devices.service";
import {TradesService} from "../../../../services/trades.service";

@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.css']
})
export class NewOrderComponent implements OnInit {

  fabButtons = [
    {
      icon: 'devices',
      action: 'device',
      label: 'Add Device'
    },
    {
      icon: 'attach_money',
      action: 'trade',
      label: 'Add Trade'
    },
  ];

  foods = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

  newOrderForm: FormGroup;
  devices: FormArray;
  trades: FormArray;

  constructor(private _formBuilder: FormBuilder,
              private _orders: OrdersService,
              private _devices: DevicesService,
              private _trades: TradesService) { }

  ngOnInit() {
    this.newOrderForm = this._formBuilder.group({
      customer: ['', Validators.required],
      city: ['', Validators.required],
      rep: ['', Validators.required],
      type: ['', Validators.required],
      leadSource: ['', Validators.required],
      paysTo: ['', Validators.required],
      salesDate: ['', Validators.required],
      purchaseLease: ['', Validators.required],
      funds: ['', Validators.required],
      servicePlan: ['', Validators.required],
      devices: this._formBuilder.array([]),
      trades: this._formBuilder.array([])
    })
  }

  createDevice(){
    return this._formBuilder.group({
      deviceModel: ['', Validators.required],
      accessories: ['', Validators.required],
      serial: ['', Validators.required],
      quantity: ['', Validators.required],
    });
  }

  addDevice(): void {
    this.devices = this.newOrderForm.get('devices') as FormArray;
    this.devices.push(this.createDevice());
  }

  createTrade() {
    return this._formBuilder.group({
      returnDisposition: ['', Validators.required],
      returnByDate: ['', Validators.required],
      model: ['', Validators.required],
      accessories: ['', Validators.required],
      serial: ['', Validators.required],
    })
  }

  addTrade():void {
    this.trades = this.newOrderForm.get('trades') as FormArray;
    this.trades.push(this.createTrade());
  }

  async onSubmit(){
    const data = this.newOrderForm.getRawValue();
    let newOrder: Order;

    const order: Order = new Order(
      null,
      data.city,
      data.customer,
      data.funds,
      data.leadSource,
      data.paysTo,
      data.purchaseLease,
      data.rep,
      data.servicePlan,
      data.type,
      data.salesDate,
      data.devices,
      data.trades
    );

   console.log(order);

   await this._orders.create(order)
      .then((response: Order) => {
        console.log(response);
        newOrder = response;
      });

   /** Map trough all devices and fire request to add them to current order */
   if (data.devices.length !== 0) {

     data.devices.map((x) => {

       const device: Device = x;

       /** Request itself */
       this._devices.addDevice(device, 1)
         .then((response) => {
           console.log(response);
         });
      });
   }
   /** */

   /** Map through all trades and fire request to add them to current order */
   if (data.trades.length !== 0) {

     data.trades.map((x)=> {

        const trade: Trade = x;

        /** Request itself*/
        this._trades.addTrade(trade, 1)
          .then((response) => {
            console.log(response);
          })
      });
   }
   /** */
  }
  /** */

  openTradeModal() {
    console.log('trade');
    this.addTrade();
  }

  openDeviceModal() {
    console.log('device');
    this.addDevice();
  }

}
