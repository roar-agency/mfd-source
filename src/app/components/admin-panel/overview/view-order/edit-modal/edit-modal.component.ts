import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material";
import {MatDialogRef} from "@angular/material";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Order} from "../../../../../models/order.model";

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.css']
})

export class EditModalComponent implements OnInit {

  orderForm: FormGroup;

  foods = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

  constructor(
    public dialogRef: MatDialogRef<EditModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EditModalComponent | any,
    private _formBuilder: FormBuilder
    ) {}

  ngOnInit() {
    this.formInit();
    this.setValue();
  }

  /** Initialize the form */
  formInit(){
    this.orderForm = this._formBuilder.group({
      rep: ['', Validators.required],
      city: ['', Validators.required],
      type: ['', Validators.required],
      leadSource: ['', Validators.required],
      paysTo: ['', Validators.required],
      salesDate: ['', Validators.required],
      purchaseLease: ['', Validators.required],
      servicePlan: ['', Validators.required],
      funds: ['', Validators.required]
    });
  }

  /** Patch order's data to existing form
   * to have what to edit.
   * */
  setValue(): void {
    if (this.data.order) {
      this.orderForm.patchValue(this.data.order);
      this.orderForm.get('salesDate').patchValue(new Date(this.data.order.salesDate));
    }
  }

  /** Send user input to parent component when form was submited */
  onSubmit() {
    this.dialogRef.close(this.orderForm.getRawValue());
  }

}
