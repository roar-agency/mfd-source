import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from "@angular/core/testing";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import {
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MAT_DIALOG_DATA
  } from "@angular/material";

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule, MatDialogRef } from '@angular/material';  
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { By } from '@angular/platform-browser';
import { EditModalComponent } from './edit-modal.component';

describe('EditModalComponent Component', () => {

    let component: EditModalComponent;
    let fixture: ComponentFixture<EditModalComponent>;
    let element: HTMLElement;

    const dialogData = {
       name: 'Edit Order'
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            
            declarations: [
                EditModalComponent
            ],
            
            imports: [
                BrowserAnimationsModule,
                FormsModule,
                ReactiveFormsModule,
                MatButtonModule,
                MatIconModule,
                MatFormFieldModule,
                MatInputModule,
                MatSelectModule,
                MatNativeDateModule,
                MatRadioModule,
                MatDatepickerModule,
                MatDialogModule
            ],
            providers: [
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: dialogData
                },
                {
                    provide: MatDialogRef
                },
                {  provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        }),

        fixture = TestBed.createComponent(EditModalComponent);
        component = fixture.debugElement.componentInstance;

    });


    it (`dialog should have headline 'Edit Trade'`, async(() => {
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1').textContent).toContain('Edit Order');
    }));


    it('dialog form should be invalid if at least one field missing', async(() => {

        component.orderForm.controls['rep'].setValue('');
        component.orderForm.controls['city'].setValue('');
        component.orderForm.controls['type'].setValue('');
        component.orderForm.controls['leadSource'].setValue('');
        component.orderForm.controls['funds'].setValue('');
        component.orderForm.controls['salesDate'].setValue('');
        component.orderForm.controls['paysTo'].setValue('');
        component.orderForm.controls['purchaseLease'].setValue('');
        component.orderForm.controls['servicePlan'].setValue('');

        expect(component.orderForm.valid).toBeFalsy();
    }));



    it('dialog form should be valid if all filed are filled out', async(() => {
        
        component.orderForm.controls['rep'].setValue('lorem');
        component.orderForm.controls['city'].setValue('lorem');
        component.orderForm.controls['type'].setValue('lorem');
        component.orderForm.controls['leadSource'].setValue(new Date('11-09-2019'));
        component.orderForm.controls['funds'].setValue('lorem');
        component.orderForm.controls['salesDate'].setValue(new Date('11-09-2019'));
        component.orderForm.controls['paysTo'].setValue('lorem');
        component.orderForm.controls['purchaseLease'].setValue('lorem');
        component.orderForm.controls['servicePlan'].setValue('lorem');

        expect(component.orderForm.valid).toBeTruthy();
    }));

    

    it(`submit button shouldn't work if form is invalid`, async(() => {

        spyOn(component, 'onSubmit');
        element = fixture.debugElement.query(By.css('#submit-btn')).nativeElement;
        element.click();

        expect(component.onSubmit).toHaveBeenCalledTimes(0);
    }));


    
    it(`submit button should work if form is valid`, async(() => {

        spyOn(component, 'onSubmit');

        component.orderForm.controls['rep'].setValue('lorem');
        component.orderForm.controls['city'].setValue('lorem');
        component.orderForm.controls['type'].setValue('lorem');
        component.orderForm.controls['leadSource'].setValue(new Date('11-09-2019'));
        component.orderForm.controls['funds'].setValue('lorem');
        component.orderForm.controls['salesDate'].setValue(new Date('11-09-2019'));
        component.orderForm.controls['paysTo'].setValue('lorem');
        component.orderForm.controls['purchaseLease'].setValue('lorem');
        component.orderForm.controls['servicePlan'].setValue('lorem');

        fixture.detectChanges();

        element = fixture.debugElement.query(By.css('#submit-btn')).nativeElement;
        element.click();
        expect(component.onSubmit).toHaveBeenCalledTimes(1);
        
        element.click();
        expect(component.onSubmit).toHaveBeenCalledTimes(2);
    }));

});