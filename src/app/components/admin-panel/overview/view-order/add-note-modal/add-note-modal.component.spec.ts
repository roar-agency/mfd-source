import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from "@angular/core/testing";
import { AddNoteModalComponent } from "./add-note-modal.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatSelectModule,
    MatToolbarModule,
    MAT_DIALOG_DATA
  } from "@angular/material";
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule, MatDialogRef } from '@angular/material';  
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { By } from '@angular/platform-browser';


describe('AddNoteModal Component', () => {

    let component: AddNoteModalComponent;
    let fixture: ComponentFixture<AddNoteModalComponent>;
    let element: HTMLElement;

    const dialogData = {
       name: 'Add Note'
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            
            declarations: [
                AddNoteModalComponent
            ],
            
            imports: [
                BrowserAnimationsModule,
                FormsModule,
                ReactiveFormsModule,
                MatButtonModule,
                MatToolbarModule,
                MatIconModule,
                MatFormFieldModule,
                MatInputModule,
                MatSelectModule,
                MatNativeDateModule,
                MatDatepickerModule,
                MatDialogModule,
                MatRadioModule
            ],
            providers: [
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: dialogData
                },
                {
                    provide: MatDialogRef
                },
                {  provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });

        fixture = TestBed.createComponent(AddNoteModalComponent);
        component = fixture.debugElement.componentInstance;
    });


    it (`dialog should have headline 'Add note'`, async(()=>{
        element = fixture.debugElement.nativeElement.querySelector('h1');
        expect(element.textContent).toContain('Add Note');
    }));



    it('dialog form shoudl be invalid if at least one field missing', async(() => {
        component.form.controls['note'].setValue('');
        expect(component.form.valid).toBeFalsy();
    }));



    it('dialog form should be valid if all filed are filled out', async(() => {
        component.form.controls['note'].setValue('long');
        expect(component.form.valid).toBeTruthy();
    }));


    it(`submit button shouldn't work if form is invalid`, async(() => {

        spyOn(component, 'onSubmit');

        element = fixture.debugElement.query(By.css('#submit-btn')).nativeElement;
        element.click();

        expect(component.onSubmit).toHaveBeenCalledTimes(0);
    }));


    it(`submit button should work if form is valid`, async(() => {
        
        spyOn(component, 'onSubmit');

        component.form.controls['note'].setValue('note');
        
        fixture.detectChanges();

        element = fixture.debugElement.query(By.css('#submit-btn')).nativeElement;
        element.click();

        expect(component.onSubmit).toHaveBeenCalledTimes(1);
    }));

});