import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-edit-device-modal',
  templateUrl: './edit-device-modal.component.html',
  styleUrls: ['./edit-device-modal.component.css']
})
export class EditDeviceModalComponent implements OnInit {

  form: FormGroup;

  foods = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

  constructor(
    public dialogRef: MatDialogRef<EditDeviceModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EditDeviceModalComponent | any,
    private _formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.initForm();
    this.setValue();
  }

  /** Initialize form controls
   * with proper validators */
  initForm() {
    this.form = this._formBuilder.group({
      serial: ['', Validators.required],
      quantity: ['', Validators.required],
      warehouse: ['', Validators.required],
      receivedLoc: ['', Validators.required],
      deliveryLoc: ['', Validators.required],
      deliveryStatus: ['', Validators.required],
      poNumber: ['', Validators.required],
      expectedDate: ['', Validators.required]
    });
  }

  /** Patch device's data to existing form
   * to have what to edit.
   * */
  setValue(): void {
    if (this.data.device) {
      this.form.patchValue(this.data.device);
      this.form.get('expectedDate').patchValue(new Date(this.data.device.expectedDate));
    }
  }

  /** Send user input to parent component when form was submitted */
  onSubmit() {
    console.log(this.form.getRawValue());
    this.dialogRef.close(this.form.getRawValue());
  }

}
