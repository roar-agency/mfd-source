import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from "@angular/core/testing";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MAT_DIALOG_DATA
  } from "@angular/material";
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule, MatDialogRef } from '@angular/material';  
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { By } from '@angular/platform-browser';
import { EditDeviceModalComponent } from './edit-device-modal.component';

describe('EditDeviceModalComponent Component', () => {

    let component: EditDeviceModalComponent;
    let fixture: ComponentFixture<EditDeviceModalComponent>;
    let element: HTMLElement;

    const dialogData = {
       name: 'Edit Device'
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            
            declarations: [
                EditDeviceModalComponent
            ],
            
            imports: [
                BrowserAnimationsModule,
                FormsModule,
                ReactiveFormsModule,
                MatButtonModule,
                MatIconModule,
                MatFormFieldModule,
                MatInputModule,
                MatSelectModule,
                MatNativeDateModule,
                MatRadioModule,
                MatDatepickerModule,
                MatDialogModule
            ],
            providers: [
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: dialogData
                },
                {
                    provide: MatDialogRef
                },
                {  provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        }),

        fixture = TestBed.createComponent(EditDeviceModalComponent);
        component = fixture.debugElement.componentInstance;

    });


    it (`dialog should have headline 'Edit Trade'`, async(() => {
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1').textContent).toContain('Edit Device');
    }));


    it('dialog form should be invalid if at least one field missing', async(() => {

        component.form.controls['serial'].setValue('');
        component.form.controls['quantity'].setValue('');
        component.form.controls['warehouse'].setValue('');
        component.form.controls['receivedLoc'].setValue('');
        component.form.controls['deliveryLoc'].setValue('');
        component.form.controls['deliveryStatus'].setValue('');
        component.form.controls['poNumber'].setValue('');
        component.form.controls['expectedDate'].setValue('');

        expect(component.form.valid).toBeFalsy();
    }));



    it('dialog form should be valid if all filed are filled out', async(() => {
        
        component.form.controls['serial'].setValue('lorem');
        component.form.controls['quantity'].setValue('lorem');
        component.form.controls['warehouse'].setValue('lorem');
        component.form.controls['receivedLoc'].setValue('lorem');
        component.form.controls['deliveryLoc'].setValue('lorem');
        component.form.controls['deliveryStatus'].setValue('lorem');
        component.form.controls['poNumber'].setValue('lorem');
        component.form.controls['expectedDate'].setValue(new Date('11-09-2019'));

        expect(component.form.valid).toBeTruthy();
    }));

    

    it(`submit button shouldn't work if form is invalid`, async(() => {

        spyOn(component, 'onSubmit');
        element = fixture.debugElement.query(By.css('#submit-btn')).nativeElement;
        element.click();

        expect(component.onSubmit).toHaveBeenCalledTimes(0);
    }));


    
    it(`submit button should work if form is valid`, async(() => {

        spyOn(component, 'onSubmit');

        component.form.controls['serial'].setValue('lorem');
        component.form.controls['quantity'].setValue('lorem');
        component.form.controls['warehouse'].setValue('lorem');
        component.form.controls['receivedLoc'].setValue('lorem');
        component.form.controls['deliveryLoc'].setValue('lorem');
        component.form.controls['deliveryStatus'].setValue('lorem');
        component.form.controls['poNumber'].setValue('lorem');
        component.form.controls['expectedDate'].setValue(new Date('11-09-2019'));

        fixture.detectChanges();

        element = fixture.debugElement.query(By.css('#submit-btn')).nativeElement;
        element.click();
        expect(component.onSubmit).toHaveBeenCalledTimes(1);
        
        element.click();
        expect(component.onSubmit).toHaveBeenCalledTimes(2);
    }));

});