import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from "@angular/core/testing";
import { AddTaskModalComponent } from "./add-task-modal.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatSelectModule,
    MatToolbarModule,
    MAT_DIALOG_DATA
  } from "@angular/material";
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule, MatDialogRef } from '@angular/material';  
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { By } from '@angular/platform-browser';


describe('AddTaskModal Component', () => {

    let component: AddTaskModalComponent;
    let fixture: ComponentFixture<AddTaskModalComponent>;
    let element: HTMLElement;

    const dialogData = {
       name: 'Add Task'
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            
            declarations: [
                AddTaskModalComponent
            ],
            
            imports: [
                BrowserAnimationsModule,
                FormsModule,
                ReactiveFormsModule,
                MatButtonModule,
                MatToolbarModule,
                MatIconModule,
                MatFormFieldModule,
                MatInputModule,
                MatSelectModule,
                MatNativeDateModule,
                MatDatepickerModule,
                MatDialogModule
            ],
            providers: [
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: dialogData
                },
                {
                    provide: MatDialogRef
                },
                {  provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });

        fixture = TestBed.createComponent(AddTaskModalComponent);
        component = fixture.debugElement.componentInstance;
    });



    it (`dialog should have headline 'Add task'`, async(()=>{
        element = fixture.debugElement.nativeElement.querySelector('h1');
        expect(element.textContent).toContain('Add Task');
    }));



    it('dialog form shoudl be invalid if at least one field missing', async(() => {
        
        component.form.controls['device'].setValue('');
        component.form.controls['leaseTerm'].setValue('');
        component.form.controls['dueDate'].setValue('');
        component.form.controls['note'].setValue('');
        expect(component.form.valid).toBeFalsy();

        component.form.controls['device'].setValue('device');
        component.form.controls['leaseTerm'].setValue('lease-term');
        component.form.controls['dueDate'].setValue(new Date('11-09-2019'));
        component.form.controls['note'].setValue('');
        expect(component.form.valid).toBeFalsy();

        component.form.controls['device'].setValue('device');
        component.form.controls['leaseTerm'].setValue('lease-term');
        component.form.controls['dueDate'].setValue('');
        component.form.controls['note'].setValue('note');
        expect(component.form.valid).toBeFalsy();

        component.form.controls['device'].setValue('device');
        component.form.controls['leaseTerm'].setValue('');
        component.form.controls['dueDate'].setValue(new Date('11-09-2019'));
        component.form.controls['note'].setValue('note');
        expect(component.form.valid).toBeFalsy();

        component.form.controls['device'].setValue('');
        component.form.controls['leaseTerm'].setValue('lease-term');
        component.form.controls['dueDate'].setValue(new Date('11-09-2019'));
        component.form.controls['note'].setValue('note');
        expect(component.form.valid).toBeFalsy();

    }));



    it('dialog form should be valid if all filed are filled out', async(() => {

        component.form.controls['device'].setValue('device');
        component.form.controls['leaseTerm'].setValue('long');
        component.form.controls['dueDate'].setValue(new Date('11-09-2019'));
        component.form.controls['note'].setValue('some notes');

        expect(component.form.valid).toBeTruthy();
    }));


    it(`submit button shouldn't work if form is invalid`, async(() => {

        spyOn(component, 'onSubmit');

        element = fixture.debugElement.query(By.css('#submit-btn')).nativeElement;
        element.click();

        expect(component.onSubmit).toHaveBeenCalledTimes(0);
    }));


    it(`submit button should work if form is valid`, async(() => {
        
        spyOn(component, 'onSubmit');

        component.form.controls['device'].setValue('device');
        component.form.controls['leaseTerm'].setValue('long');
        component.form.controls['dueDate'].setValue(new Date('11-09-2019'));
        component.form.controls['note'].setValue('some notes');
        
        fixture.detectChanges();

        element = fixture.debugElement.query(By.css('#submit-btn')).nativeElement;
        element.click();

        expect(component.onSubmit).toHaveBeenCalledTimes(1);
    }));

});