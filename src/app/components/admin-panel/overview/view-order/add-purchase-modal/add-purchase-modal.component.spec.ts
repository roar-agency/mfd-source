import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from "@angular/core/testing";
import { AddPurchaseModalComponent } from "./add-purchase-modal.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatSelectModule,
    MatToolbarModule,
    MAT_DIALOG_DATA
  } from "@angular/material";
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule, MatDialogRef } from '@angular/material';  
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { By } from '@angular/platform-browser';


describe('AddPurchaseModal Component', () => {

    let component: AddPurchaseModalComponent;
    let fixture: ComponentFixture<AddPurchaseModalComponent>;
    let element: HTMLElement;

    const dialogData = {
       name: 'Add Purchase'
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            
            declarations: [
                AddPurchaseModalComponent
            ],
            
            imports: [
                BrowserAnimationsModule,
                FormsModule,
                ReactiveFormsModule,
                MatButtonModule,
                MatToolbarModule,
                MatIconModule,
                MatFormFieldModule,
                MatInputModule,
                MatSelectModule,
                MatNativeDateModule,
                MatDatepickerModule,
                MatDialogModule,
                MatRadioModule
            ],
            providers: [
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: dialogData
                },
                {
                    provide: MatDialogRef
                },
                {  provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });

        fixture = TestBed.createComponent(AddPurchaseModalComponent);
        component = fixture.debugElement.componentInstance;
    });



    it (`dialog should have headline 'Add purchase'`, async(()=>{
        element = fixture.debugElement.nativeElement.querySelector('h1');
        expect(element.textContent).toContain('Add Purchase');
    }));

    it('dialog form shoudl be invalid if at least one field missing', async(() => {
        
        component.purchaseForm.controls['device'].setValue('device');
        component.purchaseForm.controls['placedDate'].setValue('');
        component.purchaseForm.controls['deliveryDate'].setValue('');
        component.purchaseForm.controls['poNumber'].setValue('');

        expect(component.purchaseForm.valid).toBeFalsy();

        component.purchaseForm.controls['device'].setValue('');
        component.purchaseForm.controls['placedDate'].setValue(new Date('11-09-2019'));
        component.purchaseForm.controls['deliveryDate'].setValue(new Date('11-09-2019'));
        component.purchaseForm.controls['poNumber'].setValue('12');

        expect(component.purchaseForm.valid).toBeFalsy();
    }));



    it('dialog form should be valid if all filed are filled out', async(() => {

        component.purchaseForm.controls['device'].setValue('long');
        component.purchaseForm.controls['placedDate'].setValue(new Date('11-09-2019'));
        component.purchaseForm.controls['deliveryDate'].setValue(new Date('11-09-2019'));
        component.purchaseForm.controls['poNumber'].setValue('12');

        expect(component.purchaseForm.valid).toBeTruthy();
    }));


    it(`submit button shouldn't work if form is invalid`, async(() => {

        spyOn(component, 'onSubmit');

        element = fixture.debugElement.query(By.css('#submit-btn')).nativeElement;
        element.click();

        expect(component.onSubmit).toHaveBeenCalledTimes(0);
    }));


    it(`submit button should work if form is valid`, async(() => {
        
        spyOn(component, 'onSubmit');

        component.purchaseForm.controls['device'].setValue('long');
        component.purchaseForm.controls['placedDate'].setValue(new Date('11-09-2019'));
        component.purchaseForm.controls['deliveryDate'].setValue(new Date('11-09-2019'));
        component.purchaseForm.controls['poNumber'].setValue('12');
        
        fixture.detectChanges();

        element = fixture.debugElement.query(By.css('#submit-btn')).nativeElement;
        element.click();

        expect(component.onSubmit).toHaveBeenCalledTimes(1);
    }));

});