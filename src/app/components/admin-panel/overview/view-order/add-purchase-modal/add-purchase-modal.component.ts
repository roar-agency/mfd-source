import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-add-purchase-modal',
  templateUrl: './add-purchase-modal.component.html',
  styleUrls: ['./add-purchase-modal.component.css']
})
export class AddPurchaseModalComponent implements OnInit {

  purchaseForm: FormGroup;

  foods = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

  constructor(
    public dialogRef: MatDialogRef<AddPurchaseModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AddPurchaseModalComponent,
    private _formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.purchaseForm = this._formBuilder.group({
      device: ['', Validators.required],
      poNumber: ['', Validators.required],
      dropStatus: [''],
      placedDate: ['', Validators.required],
      deliveryDate: ['', Validators.required]
    })
  }
  onSubmit() {
    console.log('Submited');
    this.dialogRef.close(this.purchaseForm.getRawValue());
  }

}
