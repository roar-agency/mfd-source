import { Component, OnInit, TemplateRef } from '@angular/core';
import {MatDialog} from "@angular/material";
import {EditModalComponent} from "./edit-modal/edit-modal.component";
import {EditTradeModalComponent} from "./edit-trade-modal/edit-trade-modal.component";
import {AddLeaseModalComponent} from "./add-lease-modal/add-lease-modal.component";
import {AddPurchaseModalComponent} from "./add-purchase-modal/add-purchase-modal.component";
import {AddReminderModalComponent} from "./add-reminder-modal/add-reminder-modal.component";
import {AddTaskModalComponent} from "./add-task-modal/add-task-modal.component";
import {AddNoteModalComponent} from "./add-note-modal/add-note-modal.component";
import {EditDeviceModalComponent} from "./edit-device-modal/edit-device-modal.component";
import {OrdersService} from "../../../../services/orders.service";
import {Order} from "../../../../models/order.model";
import {Device} from "../../../../models/device.type";
import {Validators} from "@angular/forms";
import {DevicesService} from "../../../../services/devices.service";
import {Trade} from "../../../../models/trade.type";
import {TradesService} from "../../../../services/trades.service";
import { ComponentType } from '@angular/core/src/render3';

@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.component.html',
  styleUrls: ['./view-order.component.css']
})
export class ViewOrderComponent implements OnInit {

  /** Modal components import. To make it possible to use one functions to call all of them.
   * For DRY sake */
  EditTradeModalComponent = EditTradeModalComponent;
  EditDeviceModalComponent = EditDeviceModalComponent;
  EditModalComponent = EditModalComponent;
  AddLeaseModalComponent = AddLeaseModalComponent;
  AddPurchaseModalComponent = AddPurchaseModalComponent;
  AddReminderModalComponent = AddReminderModalComponent;
  AddTaskModalComponent = AddTaskModalComponent;
  AddNoteModalComponent = AddNoteModalComponent;

  /** Order that will be shown on the page */
  order: Order;

  /** Device that will be shown on the page */
  device: Device | any;

  /** Trade that will be shown on the page */
  trade: Trade | any;

  
  constructor(public dialog: MatDialog,
              private _orders: OrdersService,
              private _devices: DevicesService,
              private _trades: TradesService) { }

  ngOnInit() {
    this.getOrder();
  }

  /** Get main info */
  getOrder() {
    this.order = this._orders.getOrder(0);
    this.device = this._devices.find(0);
    this.trade = this._trades.find(0);

  }

  /** Function used to open any modal we want depends on event
   *  that comes from fab-button.
   *  To see events - see html template file.
   */
  openModalFunction(data: object,
                    component: ComponentType<any> | any,
                    handler: (data: object, scope: ViewOrderComponent) => void ):void {
    
    const dialogRef = this.dialog.open(component, {
      width: '550px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      handler(result, this);
    });
  }

  /** Edit order function. 
   *  It works as handler argument in openModalFunction
   *  Because of this handler comes from outside the component we need to pass
   *  this component scope to this function and all next functions that we
   *  used when modal get closed. (Scope variable).
   */
  editOrder(data: any, scope: ViewOrderComponent):void {

    let order: Order = new Order(
      0,
      data.city,
      scope.order.customer,
      data.funds,
      data.leadSource,
      data.paysTo,
      data.purchaseLease,
      data.rep,
      data.servicePlan,
      data.type,
      data.salesDate,
      scope.order.devices,
      scope.order.trades);

    console.log(order);

    scope._orders.editOrder(order, 0)
      .then((response) => {
        console.log(response);
      });
  }

  /** */
  editDevice(data: any, scope: ViewOrderComponent): void {
    console.log('Edit Device');

    scope._devices.editDevice(data, 0)
      .then((response) => {
        console.log(response);
      });
  }

  /** */
  addDevice(data: any, scope: ViewOrderComponent): void  {
    console.log('Add Device');
    console.log(data);
    console.log(scope);
  }

  /** */
  editTrade(data: any, scope: ViewOrderComponent): void  {
    console.log('Edit Trade');
    console.log(data);
    console.log(scope);
  }

  /** */
  addTrade(data: any, scope: ViewOrderComponent): void  {
    console.log('Add Trade');
    console.log(data);
    console.log(scope);
  }

  /** */
  addReminder(data: any, scope: ViewOrderComponent): void  {
    console.log('addReminder');
    console.log(data);
    console.log(scope);
  }

  /** */
  addNote(data: any, scope: ViewOrderComponent): void  {
    console.log('Add note');
    console.log(data);
    console.log(scope);
  }

  /** */
  addTask(data: any, scope: ViewOrderComponent): void  {
    console.log('Add Task');
    console.log(data);
    console.log(scope);
  }

  /** */
  addLease(data: any, scope: ViewOrderComponent): void  {
    console.log('Add Lease');
    console.log(data);
    console.log(scope);
  }

  /** */
  addPurchase(data: any, scope: ViewOrderComponent): void  {
    console.log('Add Purchase');
    console.log(data);
    console.log(scope);
  }
}
