import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-purchased-orders',
  templateUrl: './purchased-orders.component.html',
  styleUrls: ['./purchased-orders.component.css']
})
export class PurchasedOrdersComponent implements OnInit {

  displayedColumns: string[] = [
    'id',
    'name',
    'soDate',
    'customer',
    'rep',
    'status'
  ];
  // columns that we will display on front-end. we should have each of this ids in html template

  dataSource = new MatTableDataSource(ELEMENT_DATA);
  
  // data that will be displayed
  data: any[] = ELEMENT_DATA;

  positionFilter = new FormControl();

  constructor() {}

  ngOnInit() {}

}

// declaration of data interface and data itself
// need to be changed when actual data will be available

export interface DataRow {
  id: string,
  name: string,
  soDate: string,
  customer: string,
  rep: string,
  status: string,
}

const ELEMENT_DATA: DataRow[] = [
  {
    id: '1',
    name: 'Lease',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '0 of 1 delivered'
  }];

