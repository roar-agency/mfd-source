import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from "@angular/core/testing";
import { ViewOrderComponent } from "./view-order.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MAT_DIALOG_DATA
  } from "@angular/material";
import { MatListModule } from '@angular/material/list';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule, MatDialogRef } from '@angular/material';  
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { OrdersService } from "../../../../services/orders.service";
import { MatExpansionModule } from '@angular/material/expansion';

// Mocks import 
import { MockFabButtonComponent } from "./mocksForTesting/MockFabBtn.component";
import { MockPurchasedOrdersComponent } from "./mocksForTesting/MockPurchasedOrders.component";
import { DevicesService } from "src/app/services/devices.service";
import { TradesService } from "src/app/services/trades.service";
import { By } from '@angular/platform-browser';
import { DebugElement } from "@angular/core";

describe('ViewOrderComponent', () => {

    let component: ViewOrderComponent;
    let fixture: ComponentFixture<ViewOrderComponent>;
    let element: HTMLElement;
    let elements: DebugElement[];

    let _orders: OrdersService;
    let _devices: DevicesService;
    let _trades: TradesService;

    const dialogData = {
       name: 'Edit Trade'
    };

    class orderServiceStub {
        getOrder(){};
    };

    class deviceServiceSttub {
        find(){};
    };

    class tradesServiceSttub {
        find(){};
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            
            declarations: [
                ViewOrderComponent,
                MockPurchasedOrdersComponent,
                MockFabButtonComponent
            ],
            
            imports: [
                BrowserAnimationsModule,
                FormsModule,
                ReactiveFormsModule,
                MatButtonModule,
                MatIconModule,
                MatFormFieldModule,
                MatInputModule,
                MatSelectModule,
                MatNativeDateModule,
                MatRadioModule,
                MatDatepickerModule,
                MatDialogModule,
                MatListModule,
                MatExpansionModule
            ],
            providers: [
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: dialogData
                },
                {
                    provide: MatDialogRef
                },
                { provide: ComponentFixtureAutoDetect, useValue: true },
                { provide: OrdersService, useClass: orderServiceStub },
                { provide: DevicesService, useClass: deviceServiceSttub },
                { provide: TradesService, useClass: tradesServiceSttub }
            ]
        }),

        fixture = TestBed.createComponent(ViewOrderComponent);
        component = fixture.debugElement.componentInstance;

        _orders = fixture.debugElement.injector.get(OrdersService);
        _devices = fixture.debugElement.injector.get(DevicesService);
        _trades = fixture.debugElement.injector.get(TradesService);
    });

    it (`component should have headline 'Some customer'`, async(() => {
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1').textContent).toContain('Some customer');
    }));


    it('component should get order from order service', async(() => {
        expect(_orders.getOrder(0)).toEqual(component.order);
    }));


    it('component should get device from devices service', async(() => {
        expect(_devices.find(0)).toEqual(component.device);
    }));


    it('component should get trade from tradess service', async(() => {
        expect(_trades.find(0)).toEqual(component.trade);
    }));

    
    it(`all edit buttons should emit openModal event`, async(() => {

        spyOn(component, 'openModalFunction');
        elements = fixture.debugElement.queryAll(By.css('.open-modal'));
        
        elements.map(x => x.nativeElement.click());
        expect(component.openModalFunction).toHaveBeenCalledTimes(elements.length);
    
    }));

});