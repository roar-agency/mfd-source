import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from "@angular/core/testing";
import { AddLeaseModalComponent } from "./add-lease-modal.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatSelectModule,
    MatToolbarModule,
    MAT_DIALOG_DATA
  } from "@angular/material";
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule, MatDialogRef } from '@angular/material';  
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { By } from '@angular/platform-browser';


describe('AddLeasekModal Component', () => {

    let component: AddLeaseModalComponent;
    let fixture: ComponentFixture<AddLeaseModalComponent>;
    let element: HTMLElement;

    const dialogData = {
       name: 'Add Lease'
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            
            declarations: [
                AddLeaseModalComponent
            ],
            
            imports: [
                BrowserAnimationsModule,
                FormsModule,
                ReactiveFormsModule,
                MatButtonModule,
                MatToolbarModule,
                MatIconModule,
                MatFormFieldModule,
                MatInputModule,
                MatSelectModule,
                MatNativeDateModule,
                MatDatepickerModule,
                MatDialogModule,
                MatRadioModule
            ],
            providers: [
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: dialogData
                },
                {
                    provide: MatDialogRef
                },
                {  provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });

        fixture = TestBed.createComponent(AddLeaseModalComponent);
        component = fixture.debugElement.componentInstance;
    });

    it (`dialog should have headline 'Add lease'`, async(()=>{
        element = fixture.debugElement.nativeElement.querySelector('h1');
        expect(element.textContent).toContain('Add Lease');
    }));

    it('dialog form shoudl be invalid if at least one field missing', async(() => {
        
        component.leaseForm.controls['device'].setValue('device');
        component.leaseForm.controls['signDate'].setValue('');
        component.leaseForm.controls['fundedDate'].setValue('');
        component.leaseForm.controls['finProvider'].setValue('');
        component.leaseForm.controls['leaseTerm'].setValue('');

        expect(component.leaseForm.valid).toBeFalsy();

        component.leaseForm.controls['device'].setValue('');
        component.leaseForm.controls['signDate'].setValue(new Date('11-09-2019'));
        component.leaseForm.controls['fundedDate'].setValue(new Date('11-09-2019'));
        component.leaseForm.controls['finProvider'].setValue('12');
        component.leaseForm.controls['leaseTerm'].setValue('12');

        expect(component.leaseForm.valid).toBeFalsy();
    }));



    it('dialog form should be valid if all filed are filled out', async(() => {

        component.leaseForm.controls['device'].setValue('long');
        component.leaseForm.controls['signDate'].setValue(new Date('11-09-2019'));
        component.leaseForm.controls['fundedDate'].setValue(new Date('11-09-2019'));
        component.leaseForm.controls['finProvider'].setValue('12');
        component.leaseForm.controls['leaseTerm'].setValue('12');

        expect(component.leaseForm.valid).toBeTruthy();
    }));


    it(`submit button shouldn't work if form is invalid`, async(() => {

        spyOn(component, 'onSubmit');

        element = fixture.debugElement.query(By.css('#submit-btn')).nativeElement;
        element.click();

        expect(component.onSubmit).toHaveBeenCalledTimes(0);
    }));


    it(`submit button should work if form is valid`, async(() => {
        
        spyOn(component, 'onSubmit');

        component.leaseForm.controls['device'].setValue('long');
        component.leaseForm.controls['signDate'].setValue(new Date('11-09-2019'));
        component.leaseForm.controls['fundedDate'].setValue(new Date('11-09-2019'));
        component.leaseForm.controls['finProvider'].setValue('12');
        component.leaseForm.controls['leaseTerm'].setValue('12');
        
        fixture.detectChanges();

        element = fixture.debugElement.query(By.css('#submit-btn')).nativeElement;
        element.click();

        expect(component.onSubmit).toHaveBeenCalledTimes(1);
    }));

});