import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-add-lease-modal',
  templateUrl: './add-lease-modal.component.html',
  styleUrls: ['./add-lease-modal.component.css']
})
export class AddLeaseModalComponent implements OnInit {

  leaseForm: FormGroup;

  foods = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

  constructor(
    public dialogRef: MatDialogRef<AddLeaseModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AddLeaseModalComponent,
    private _formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.leaseForm = this._formBuilder.group({
      device: ['', Validators.required],
      finProvider: ['', Validators.required],
      leaseTerm: ['', Validators.required],
      signDate: ['', Validators.required],
      fundedDate: ['', Validators.required]
    })
  }

  onSubmit() {
    console.log('Submited');
    this.dialogRef.close(this.leaseForm.getRawValue());
  }

}
