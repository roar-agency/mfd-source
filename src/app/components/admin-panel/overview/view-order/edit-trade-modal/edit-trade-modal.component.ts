import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-edit-trade-modal',
  templateUrl: './edit-trade-modal.component.html',
  styleUrls: ['./edit-trade-modal.component.css']
})
export class EditTradeModalComponent implements OnInit {

  form: FormGroup;

  foods = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

  constructor(
    public dialogRef: MatDialogRef<EditTradeModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EditTradeModalComponent | any,
    private _formBuilder: FormBuilder
  ) {}

  ngOnInit() {
   this.formInit();
   this.setValue();
  }

  /** Initialize form controls with proper Validators */
  formInit() {
    this.form = this._formBuilder.group({
      model: ['', Validators.required],
      serial: ['', Validators.required],
      returnDisposition: ['', Validators.required],
      returnDate: ['', Validators.required],
      funds: ['', Validators.required],
      removedDate: ['', Validators.required],
      blackMeter: ['', Validators.required],
      colorMeter: ['', Validators.required]
    })
  }

  /** Patch device's data to existing form
   * to have what to edit.
   * */
  setValue(): void {
    if (this.data.trade) {
      this.form.patchValue(this.data.trade);
      this.form.get('returnDate').patchValue(new Date(this.data.trade.returnDate));
      this.form.get('removedDate').patchValue(new Date(this.data.trade.removedDate));
    }
  }

  /** Send user input to parent component when form was submitted */
  onSubmit() {
    console.log(this.form.getRawValue());
    this.dialogRef.close(this.form.getRawValue());
  }

}
