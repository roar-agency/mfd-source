import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from "@angular/core/testing";
import { EditTradeModalComponent } from "./edit-trade-modal.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MAT_DIALOG_DATA
  } from "@angular/material";
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule, MatDialogRef } from '@angular/material';  
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { By } from '@angular/platform-browser';

describe('EditTradeModal Component', () => {

    let component: EditTradeModalComponent;
    let fixture: ComponentFixture<EditTradeModalComponent>;
    let element: HTMLElement;

    const dialogData = {
       name: 'Edit Trade'
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            
            declarations: [
                EditTradeModalComponent
            ],
            
            imports: [
                BrowserAnimationsModule,
                FormsModule,
                ReactiveFormsModule,
                MatButtonModule,
                MatIconModule,
                MatFormFieldModule,
                MatInputModule,
                MatSelectModule,
                MatNativeDateModule,
                MatRadioModule,
                MatDatepickerModule,
                MatDialogModule
            ],
            providers: [
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: dialogData
                },
                {
                    provide: MatDialogRef
                },
                {  provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        }),

        fixture = TestBed.createComponent(EditTradeModalComponent);
        component = fixture.debugElement.componentInstance;

    });

    it (`dialog should have headline 'Edit Trade'`, async(() => {
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1').textContent).toContain('Edit Trade');
    }));


    it('dialog form should be invalid if at least one field missing', async(() => {

        component.form.controls['model'].setValue('');
        component.form.controls['serial'].setValue('');
        component.form.controls['returnDisposition'].setValue('');
        component.form.controls['returnDate'].setValue('');
        component.form.controls['funds'].setValue('');
        component.form.controls['removedDate'].setValue('');
        component.form.controls['blackMeter'].setValue('');
        component.form.controls['colorMeter'].setValue('');

        expect(component.form.valid).toBeFalsy();
    }));



    it('dialog form should be valid if all filed are filled out', async(() => {
    
        component.form.controls['model'].setValue('lorem');
        component.form.controls['serial'].setValue('lorem');
        component.form.controls['returnDisposition'].setValue('lorem');
        component.form.controls['returnDate'].setValue(new Date('11-09-2019'));
        component.form.controls['funds'].setValue('lorem');
        component.form.controls['removedDate'].setValue(new Date('11-09-2019'));
        component.form.controls['blackMeter'].setValue('lorem');
        component.form.controls['colorMeter'].setValue('lorem');

        expect(component.form.valid).toBeTruthy();
    }));

    

    it(`submit button shouldn't work if form is invalid`, async(() => {

        spyOn(component, 'onSubmit');
        element = fixture.debugElement.query(By.css('#submit-btn')).nativeElement;
        element.click();

        expect(component.onSubmit).toHaveBeenCalledTimes(0);
    }));



    it(`submit button should work if form is valid`, async(() => {

        spyOn(component, 'onSubmit');

        component.form.controls['model'].setValue('lorem');
        component.form.controls['serial'].setValue('lorem');
        component.form.controls['returnDisposition'].setValue('lorem');
        component.form.controls['returnDate'].setValue(new Date('11-09-2019'));
        component.form.controls['funds'].setValue('lorem');
        component.form.controls['removedDate'].setValue(new Date('11-09-2019'));
        component.form.controls['blackMeter'].setValue('lorem');
        component.form.controls['colorMeter'].setValue('lorem');

        fixture.detectChanges();

        element = fixture.debugElement.query(By.css('#submit-btn')).nativeElement;
        element.click();

        expect(component.onSubmit).toHaveBeenCalledTimes(1);
    }));

});