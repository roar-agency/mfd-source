import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material";
import {FormControl} from "@angular/forms";
import {SearchService} from "../../../../services/search.service";
import {TradesService} from "../../../../services/trades.service";

@Component({
  selector: 'app-trades',
  templateUrl: './trades.component.html',
  styleUrls: ['./trades.component.css']
})

export class TradesComponent implements OnInit {

  displayedColumns: string[] = [
    'id',
    'customer',
    'rep',
    'retDisposition',
    'dispositionNote',
    'returnBy',
    'received',
    'removed'
  ];
  // columns that we will display on front-end. we should have each of this ids in html template

  dataSource = new MatTableDataSource(ELEMENT_DATA);
  // data that will be displayed
  data: any[] = ELEMENT_DATA;

  positionFilter = new FormControl();

  constructor(private _search: SearchService,
              private _trades: TradesService) {}

  ngOnInit() {

    // subscribe to listen filter value
    this.positionFilter.valueChanges
      .subscribe(value => {
        this.dataSource.filter = value;
      });

    // custom filter function for table data:
    this.dataSource.filterPredicate = this._search.createFilter();
  }

  /** Get data to use it in table. Should get model for order later */
  getData() {

    this._trades.get()
      .then((response) => {
        console.log(response);
        // this.dataSource = response;
      });
  }
  /** */
}

// declaration of data interface and data itself
// need to be changed when actual data will be available

export interface DataRow {
  id: string,
  customer: string,
  rep: string,
  retDisposition: string,
  dispositionNote: string,
  returnBy: string,
  received: string,
  removed: string
}

const ELEMENT_DATA: DataRow[] = [
  {
    id: '1',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    retDisposition: 'XFS Return',
    dispositionNote: '-',
    returnBy: '07/11/2018',
    received: '08/11/2018',
    removed: 'n/a/'
  },
  {
    id: '2',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    retDisposition: 'XFS Return',
    dispositionNote: '-',
    returnBy: '07/11/2018',
    received: '08/11/2018',
    removed: 'n/a/'
  },
  {
    id: '3',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    retDisposition: 'XFS Return',
    dispositionNote: '-',
    returnBy: '07/11/2018',
    received: '08/11/2018',
    removed: 'n/a/'
  },{
    id: '4',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    retDisposition: 'XFS Return',
    dispositionNote: '-',
    returnBy: '07/11/2018',
    received: '08/11/2018',
    removed: 'n/a/'
  },{
    id: '213',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    retDisposition: 'XFS Return',
    dispositionNote: '-',
    returnBy: '07/11/2018',
    received: '08/11/2018',
    removed: 'n/a/'
  },{
    id: '223',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    retDisposition: 'XFS Return',
    dispositionNote: '-',
    returnBy: '07/11/2018',
    received: '08/11/2018',
    removed: 'n/a/'
  },{
    id: '342',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    retDisposition: 'XFS Return',
    dispositionNote: '-',
    returnBy: '07/11/2018',
    received: '08/11/2018',
    removed: 'n/a/'
  },
];

