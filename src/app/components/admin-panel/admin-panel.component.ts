import { Component } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from "../../services/auth.service";
import { RouterOutlet } from "@angular/router";
import { RouteAnimation } from "../../animations/route-animation";

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css'],
  animations: [
    RouteAnimation
  ]
})

export class AdminPanelComponent {

  isBigEnough$: Observable<boolean> = this.breakpointObserver.observe('(min-width: 1441px)')
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,
              private _auth: AuthService) {}

  logOut() {
    this._auth.logOut();
  }

  /** Route animations detect method */
  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

}
