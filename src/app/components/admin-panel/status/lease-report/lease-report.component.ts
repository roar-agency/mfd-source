import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-lease-report',
  templateUrl: './lease-report.component.html',
  styleUrls: ['./lease-report.component.css']
})
export class LeaseReportComponent implements OnInit {

  displayedColumns: string[] = [
    'id',
    'customer',
    'rep',
    'soDate',
    'poDate',
    'warehouse',
    'delivery',
    'dA',
    'funded',
    'status'
  ];
  // columns that we will display on front-end. we should have each of this ids in html template

  dataSource = new MatTableDataSource(ELEMENT_DATA);
  // data that will be displayed
  data: any[] = ELEMENT_DATA;

  positionFilter = new FormControl();

  constructor() {}

  ngOnInit() {

    // subscribe to listen filter value
    this.positionFilter.valueChanges
      .subscribe(value => {
        this.dataSource.filter = value;
      });

    // custom filter function for table data:
    this.dataSource.filterPredicate = this.createFilter();

  }

  createFilter() {

    console.log('create Good FilterFunc');

    const filterFunction = (data, filter: string): boolean => {

      filter = filter.toLowerCase();

      const matchFilter = [];
      const filterArray = filter.split(',');
      // const columns = [data.id, data.company, data.advertiser, data.product];
      // Or if you don't want to specify specifics columns =>
      const columns = (<any>Object).values(data);

      // Main loop
      filterArray.forEach((fil) => {
        const customFilter = [];
        columns.forEach((column) => {
          customFilter.push(column.toLowerCase().includes(fil));
        });
        matchFilter.push(customFilter.some(Boolean)); // OR
      });
      return matchFilter.every(Boolean); // AND
    };
    return filterFunction;
  }
  // end of filter function
}

// declaration of data interface and data itself
// need to be changed when actual data will be available

export interface DataRow {
  id: string,
  customer: string,
  rep: string,
  soDate: string,
  poDate: string,
  warehouse: string,
  delivery: string,
  dA: string,
  funded: string,
  status: string
}

const ELEMENT_DATA: DataRow[] = [
  {
    id: '1',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    soDate: '08/11/2018',
    poDate: '08/11/2018',
    warehouse: 'Pendong',
    delivery: 'not scheduled',
    dA: 'not signed',
    funded: 'not funded',
    status: '1 of 2 delivered (50%)'
  },
  {
    id: '351',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    soDate: '08/11/2018',
    poDate: '08/11/2018',
    warehouse: 'Pendong',
    delivery: 'not scheduled',
    dA: 'not signed',
    funded: 'not funded',
    status: '1 of 2 delivered (50%)'
  },
  {
    id: '1435',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    soDate: '08/11/2018',
    poDate: '08/11/2018',
    warehouse: 'Pendong',
    delivery: 'not scheduled',
    dA: 'not signed',
    funded: 'not funded',
    status: '1 of 2 delivered (50%)'
  },
  {
    id: '1324',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    soDate: '08/11/2018',
    poDate: '08/11/2018',
    warehouse: 'Pendong',
    delivery: 'not scheduled',
    dA: 'not signed',
    funded: 'not funded',
    status: '1 of 2 delivered (50%)'
  },  {
    id: '1546',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    soDate: '08/11/2018',
    poDate: '08/11/2018',
    warehouse: 'Pendong',
    delivery: 'not scheduled',
    dA: 'not signed',
    funded: 'not funded',
    status: '1 of 2 delivered (50%)'
  },  {
    id: '1645',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    soDate: '08/11/2018',
    poDate: '08/11/2018',
    warehouse: 'Pendong',
    delivery: 'not scheduled',
    dA: 'not signed',
    funded: 'not funded',
    status: '1 of 2 delivered (50%)'
  },  {
    id: '1231',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    soDate: '08/11/2018',
    poDate: '08/11/2018',
    warehouse: 'Pendong',
    delivery: 'not scheduled',
    dA: 'not signed',
    funded: 'not funded',
    status: '1 of 2 delivered (50%)'
  },  {
    id: '6431',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    soDate: '08/11/2018',
    poDate: '08/11/2018',
    warehouse: 'Pendong',
    delivery: 'not scheduled',
    dA: 'not signed',
    funded: 'not funded',
    status: '1 of 2 delivered (50%)'
  },  {
    id: '1376',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    soDate: '08/11/2018',
    poDate: '08/11/2018',
    warehouse: 'Pendong',
    delivery: 'not scheduled',
    dA: 'not signed',
    funded: 'not funded',
    status: '1 of 2 delivered (50%)'
  },  {
    id: '1646',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    soDate: '08/11/2018',
    poDate: '08/11/2018',
    warehouse: 'Pendong',
    delivery: 'not scheduled',
    dA: 'not signed',
    funded: 'not funded',
    status: '1 of 2 delivered (50%)'
  },
];




