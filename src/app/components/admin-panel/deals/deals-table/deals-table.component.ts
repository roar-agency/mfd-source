import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-deals-table',
  templateUrl: './deals-table.component.html',
  styleUrls: ['./deals-table.component.css']
})

export class DealsTableComponent implements OnInit {

  displayedColumns: string[] = [
    'id',
    'soDate',
    'customer',
    'rep',
    'status',
    'lastViewed'
  ];
  // columns that we will display on front-end. we should have each of this ids in html template

  dataSource = new MatTableDataSource(ELEMENT_DATA);
  // data that will be displayed
  data: any[] = ELEMENT_DATA;

  positionFilter = new FormControl();

  constructor() {}

  ngOnInit() {

    // subscribe to listen filter value
    this.positionFilter.valueChanges
      .subscribe(value => {
        this.dataSource.filter = value;
      });

    // custom filter function for table data:
    this.dataSource.filterPredicate = this.createFilter();

  }

  createFilter() {

    console.log('create Good FilterFunc');

    const filterFunction = (data, filter: string): boolean => {

      filter = filter.toLowerCase();

      const matchFilter = [];
      const filterArray = filter.split(',');
      const columns = [data.id, data.customer, data.rep, data.soDate, data.status, data.lastViewed];
      // Or if you don't want to specify specifics columns =>
      // const columns = (<any>Object).values(data);

      // Main loop
      filterArray.forEach((fil) => {
        
        const customFilter = [];
        
        columns.forEach((column) => {
          customFilter.push(column.toLowerCase().includes(fil));
        });

        matchFilter.push(customFilter.some(Boolean)); // OR
      
      });
      
      return matchFilter.every(Boolean); // AND

    };
    return filterFunction;
  }
  // end of filter function
}

// declaration of data interface and data itself
// need to be changed when actual data will be available

export interface DataRow {
  id: string,
  soDate: string,
  customer: string,
  rep: string,
  status: string,
  lastViewed: string
}

const ELEMENT_DATA: DataRow[] = [
  {
    id: '1',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '3 of 3 delivered (100%)',
    lastViewed: '09/26/2018 2:18 pm'
  },
  {
    id: '2',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '3 of 3 delivered (100%)',
    lastViewed: '09/26/2018 2:18 pm'
  },
  {
    id: '3',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '3 of 3 delivered (100%)',
    lastViewed: '09/26/2018 2:18 pm'
  },  {
    id: '4',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '3 of 3 delivered (100%)',
    lastViewed: '09/26/2018 2:18 pm'
  },  {
    id: '5',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '3 of 3 delivered (100%)',
    lastViewed: '09/26/2018 2:18 pm'
  },  {
    id: '6',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '3 of 3 delivered (100%)',
    lastViewed: '09/26/2018 2:18 pm'
  },  {
    id: '7',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '3 of 3 delivered (100%)',
    lastViewed: '09/26/2018 2:18 pm'
  },  {
    id: '8',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '3 of 3 delivered (100%)',
    lastViewed: '09/26/2018 2:18 pm'
  },
  {
    id: '9',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '3 of 3 delivered (100%)',
    lastViewed: '09/26/2018 2:18 pm'
  },
  {
    id: '10',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '3 of 3 delivered (100%)',
    lastViewed: '09/26/2018 2:18 pm'
  },  {
    id: '11',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '3 of 3 delivered (100%)',
    lastViewed: '09/26/2018 2:18 pm'
  },  {
    id: '12',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '3 of 3 delivered (100%)',
    lastViewed: '09/26/2018 2:18 pm'
  },  {
    id: '13',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '3 of 3 delivered (100%)',
    lastViewed: '09/26/2018 2:18 pm'
  },  {
    id: '14',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '3 of 3 delivered (100%)',
    lastViewed: '09/26/2018 2:18 pm'
  },  {
    id: '15',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '3 of 3 delivered (100%)',
    lastViewed: '09/26/2018 2:18 pm'
  },
  {
    id: '16',
    soDate: '08/20/2018',
    customer: 'RRE Matthews Reserve Holdings',
    rep: 'Chris Witmer',
    status: '3 of 3 delivered (100%)',
    lastViewed: '09/26/2018 2:18 pm'
  },];

