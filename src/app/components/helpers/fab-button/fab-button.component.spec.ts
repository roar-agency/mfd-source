import { TestBed } from "@angular/core/testing";
import { FabButtonComponent } from "./fab-button.component";
import { RouterModule } from "@angular/router";

describe('Component: Fab-button', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations:[FabButtonComponent],
            imports: [RouterModule]
        });
    });

    it('should create the apfab btn component', () => {
        let fixture = TestBed.createComponent(FabButtonComponent);
        let app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    it('should show/hide content on toggle', () => {
        let fixture = TestBed.createComponent(FabButtonComponent);
        let app = fixture.debugElement.componentInstance;
        expect(app.fabTogglerState).toEqual('inactive');
        app.onToggleFab();
        expect(app.fabTogglerState).toEqual('active');
        app.onToggleFab();
        expect(app.fabTogglerState).toEqual('inactive');
    });

});