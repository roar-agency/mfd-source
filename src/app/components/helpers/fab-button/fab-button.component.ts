import { Component } from '@angular/core';
import { speedDialFabAnimations } from "../animations/fabanimation";

@Component({
  selector: 'app-fab-button',
  templateUrl: './fab-button.component.html',
  styleUrls: ['./fab-button.component.css'],
  animations: speedDialFabAnimations
})

export class FabButtonComponent {

  fabTogglerState = 'inactive';

  constructor() { }

  showItems() {
    this.fabTogglerState = 'active';
  }

  hideItems() {
    this.fabTogglerState = 'inactive';
  }

  onToggleFab() {
    this.fabTogglerState === 'active' ? this.hideItems() : this.showItems();
  }

}
