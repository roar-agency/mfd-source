import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  constructor(private _formBuilder: FormBuilder,
              private _auth: AuthService
              ) { }

  ngOnInit() {
    this.form = this._formBuilder.group({
      email: ['', Validators.required],
      pass: ['', Validators.required]
    });
  }

  onSubmit() {
    const email = this.form.get('email').value;
    const pass = this.form.get('pass').value;

    this._auth.login(email, pass);
      // .then((response) => {
      //   console.log(response);
      // })
  }

}
