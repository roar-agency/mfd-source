export class Reminder{
  constructor(public order_id: number,
              public user_id: number,
              public content: string,
              public date: string){}
}
