import {Device} from "./device.type";
import {Trade} from "./trade.type";

export class Order{

  constructor(public id: number,
              public city: string,
              public customer: string,
              public funds: number,
              public leadSource: string,
              public paysTo: string,
              public purchaseLease: string,
              public rep: string,
              public servicePlan: string,
              public type: string,
              public salesDate: string,
              public devices: Device[],
              public trades: Trade[]) {}

  /**  --- Test method ---
    addFunds() {
      this.funds += 2000;
    }
   */
}
