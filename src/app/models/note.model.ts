export class Note{
  constructor(public order_id: number,
              public user_id: number,
              public content: string) {}
}
