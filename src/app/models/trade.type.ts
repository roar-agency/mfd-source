export interface Trade {
  accessories: string,
  model: string,
  returnByDate: string,
  returnDisposition: string,
  serial: string
}
