import {Device} from "./device.type";

export class Task{
  constructor(public id: number,
              public order_id: number,
              public user_id: number,
              public device: Device,
              public leaseTerm: string,
              public dueDate: string,
              public content: string,
              public status: boolean
              ){}
}
