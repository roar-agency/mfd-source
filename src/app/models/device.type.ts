export interface Device {
  accessories: string,
  deviceModel: string,
  quantity: number,
  serial: string
}
