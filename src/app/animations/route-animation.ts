import {animate, animateChild, group, query, style, transition, trigger} from '@angular/animations';

export const RouteAnimation = trigger('routeAnimations', [

    transition(
      'HomePage => NewOrderPage, HomePage => ViewOrderPage', [

      style({
        position: 'relative',
        opacity: '1'
      }),

      query(':enter, :leave', [
          style({
            position: 'absolute',
            left: 0,
            width: '100%'
          }),
        ],
        {optional: true}
      ),

      query(':enter', [
          style({
            left: '100%',
            opacity: '0'
          })
        ],
        {optional: true}
      ),

      query(':leave', animateChild(), {optional: true}),

      group([

        query(':enter', [
            animate('500ms ease-out', style({
              opacity: '1',
              left: '0'
            }))
          ],
          { optional: true}),

        query(':leave', [
          animate('500ms ease-out', style({
            opacity: '0',
            left: '-100%'
          }))
        ], {optional: true}),


      ]),

      query(':enter', animateChild(), {optional: true}),
    ]),

  transition(
    'NewOrderPage => *, ViewOrderPage => *', [

      style({
        position: 'relative',
        opacity: '1'
      }),

      query(':enter, :leave', [
          style({
            position: 'absolute',
            left: 0,
            width: '100%'
          }),
        ],
        {optional: true}
      ),

      query(':enter', [
          style({
            left: '-100%',
            opacity: '0'
          })
        ],
        {optional: true}
      ),

      query(':leave', animateChild(), {optional: true}),

      group([

        query(':enter', [
            animate('500ms ease-out', style({
              opacity: '1',
              left: '0'
            }))
          ],
          { optional: true}),

        query(':leave', [
          animate('500ms ease-out', style({
            opacity: '0',
            left: '100%'
          }))
        ], {optional: true}),


      ]),

      query(':enter', animateChild(), {optional: true}),
    ]),


  transition(
    'HomePage <=> StatusPage, ' +
    'HomePage <=> DealsPage, ' +
    'DealsPage <=> StatusPage', [

      style({
        position: 'relative',
        opacity: '1'
      }),

      query(':enter, :leave', [
          style({
            position: 'absolute',
            left: 0,
            width: '100%'
          }),
        ],
        {optional: true}
      ),

      query(':enter', [
          style({
            opacity: '0'
          })
        ],
        {optional: true}
      ),

      query(':leave', animateChild(), {optional: true}),

      group([

        query(':enter', [
            animate('500ms ease-out', style({
              opacity: '1'
            }))
          ],
          { optional: true}),

        query(':leave', [
          animate('500ms ease-out', style({
            opacity: '0'
          }))
        ], {optional: true}),


      ]),

      query(':enter', animateChild(), {optional: true}),
    ]),

  ]);
